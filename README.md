# MPSoc on Phase-2 SL @KEK

## ディレクトリ構造。
```
|-- boot (一番プリミティブなブートファイル)
|-- boot_v2
|-- boot_v3 (PSのレジスタに触るためのブートファイル)
|-- devmem
|-- OnBoardXtal2 (PSのレジスタに触るためのsi5345のコンフィギュレーションファイル)
|-- si5345_config (一番プリミティブなコンフィギュレーションファイル)
`-- README.md
```

実行の仕方は三島さんのgit(https://gitlab.cern.ch/amishima/sl_tutorial )を参照。


