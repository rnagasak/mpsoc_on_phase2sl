#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>

int main (int argc , char* argv[]){
                      int   fd;
             unsigned int   page_size = sysconf( _SC_PAGESIZE );
             unsigned int   phys_addr, page_addr, page_offset;
             unsigned int   initial_val;
	volatile unsigned int * ptr;

    // Open /dev/mem with read-write mode.
	fd = open( "/dev/mem", O_RDWR ); // File Descriptor. fd >= 0 if successful
	if( fd < 0 ) {
		printf("Error opening /dev/mem\n" );
        exit(EXIT_FAILURE);
	}

    // Main process
    if (!strcmp(argv[1], "-r") && argc==3){ // if argv[1] == "-r" and argc==2 then ...
        volatile unsigned int input_addr = strtoul(argv[2], NULL, 0);
	phys_addr   = input_addr;
	page_addr   = ( phys_addr & ~ ( page_size - 1 ));
	page_offset = phys_addr - page_addr;
	ptr    = mmap( NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr );
	if( ptr == MAP_FAILED ) {
        printf("Failed to map physical address.\n");
        close( fd );
		return EXIT_FAILURE;
	}
	ptr += page_offset;
        initial_val = *ptr;
        printf("0x%x\n", initial_val);
	
    } else if (!strcmp(argv[1], "-w") && argc==4){
        volatile unsigned int input_addr = strtoul(argv[2], NULL, 0);
        volatile unsigned int input_val = strtoul(argv[3], NULL, 0);
	phys_addr   = input_addr;
	page_addr   = ( phys_addr & ~ ( page_size - 1 ));
	page_offset = phys_addr - page_addr;
	ptr    = mmap( NULL, page_size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, page_addr );
	if( ptr == MAP_FAILED ) {
        printf("Failed to map physical address.\n");
        close( fd );
		return EXIT_FAILURE;
	}
	ptr += page_offset;
        *ptr = input_val;
        printf("Value at address 0x%x changed: 0x%x\n", input_addr, input_val);
	
    } else{
        printf("Usage: ./devmem [-r] [-w 0xdeadbeef]\n");
        close(fd);
        return EXIT_FAILURE;
    }

    close(fd);
	return EXIT_SUCCESS;
}
